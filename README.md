# Reachability-Plugin.swift

Reachability-Plugin.swift is a replacement for Apple's Reachability sample, re-written in Swift with closures by https://github.com/ashleymills/ and adapted for me to use.

It is compatible with **iOS** (8.0 - 9.2), **OSX** (10.9 - 10.11) and **tvOS** (9.0 - 9.1)


## Installation
### Manual
Just drop the **Reachability-Plugin.swift** file into your project. That's it!

See my simple test on **ViewController.swift** with UI interaction with the Main.storyboard.



## Want to help?

Got a bug fix, or a new feature? Create a pull request and go for it!

## Let me know!

If you use **Reachability-Plugin.swift**, please let me know about your app and I'll put a link [here…](https://bitbucket.org/jack_loverde/reachability-example/README.md) and tell your friends!


Thanks,
Daniel
