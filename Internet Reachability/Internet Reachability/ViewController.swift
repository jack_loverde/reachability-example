//
//  ViewController.swift
//  Internet Reachability
//
//  Created by Daniel Arantes Loverde on 2/15/16.
//  Copyright © 2016 Loverde Co. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var headerMessage: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var btStatus: UIButton!
    var reachability: Reachability?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            //return
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: #selector(ViewController.reachabilityChanged(_:)),
            name: ReachabilityChangedNotification,
            object: reachability)
        
        do{
            try reachability!.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false

            headerMessage.text = "we have internet!!!"
            activityIndicator.stopAnimating()
            btStatus.enabled = true
            btStatus.titleLabel?.text = "YES internet"
            btStatus.backgroundColor = UIColor.greenColor()
            
        } else {
            print("Not reachable")
            
            headerMessage.text = "NO INTERNET"
            activityIndicator.startAnimating()
            btStatus.enabled = false
            btStatus.titleLabel?.text = "NO INTERNET"
            btStatus.backgroundColor = UIColor.redColor()

            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        }
    }
}

